package StepDefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.cucumber.java.en.*;

public class BDD_Login {

	WebDriver driver=null;

	@Given("User opens the browser")
	public void user_open_the_browser() {
		System.out.println("Steps - User open the browser");
		
		//disable notif browser
		ChromeOptions opt = new ChromeOptions(); 
		opt.addArguments("--disable-notifications");
		
		//chromedriver setup
		System.setProperty("webdriver.chrome.driver", "/Users/tfeldiorean/tema/chromedriver");

		//open Chrome browser
		driver = new ChromeDriver();

		//Maximize browser
		driver.manage().window().maximize();

	}

	@And("User is on demoblaze login page")
	public void user_on_demoblaze_login_page() {
		System.out.println("Steps - User on demoblaze login page");
		driver.get("https://www.demoblaze.com/index.html");
	}

	@When("User clicks on login button")
	public void user_enter_valid_email_or_phone_number() throws InterruptedException {
		System.out.println("Steps - User enter valid email or phone number");
		Thread.sleep(3000);
		//clicks on login button
	    driver.findElement(By.id("login2")).click();

	}

	@And("User enters valid credentials")
	public void user_enter_valid_password() throws InterruptedException {
		System.out.println("Steps - User enter valid credentials");
		Thread.sleep(3000);
		//input credentials
		driver.findElement(By.id("loginusername")).click();
		driver.findElement(By.id("loginusername")).sendKeys("test@test.com");
		driver.findElement(By.id("loginpassword")).click();
		driver.findElement(By.id("loginpassword")).sendKeys("test");

	}

	@And("Clicks Login button")
	public void click_login_button() {
		System.out.println("Steps - Click Login button");

		//clicks "Login button"
		driver.findElement(By.xpath("//*[@id=\"logInModal\"]/div/div/div[3]/button[2]")).click();
	}

	@Then("User navigates to cart")
	public void user_navigates_to_cart() throws InterruptedException {
		System.out.println("Steps - User navigates to cart");

		//user navigated to cart
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[contains(text(), 'Cart')]")).click();
		//close browser
		driver.close();
		driver.quit();
	}

}
