Feature: Feature test a login page
  User wants to login successfully

  Scenario: Validate login is working
    Given User opens the browser
    And User is on demoblaze login page
    When User clicks on login button
    And User enters valid credentials
    And Clicks Login button
    Then User navigates to cart
